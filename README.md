# Delorean

- **OS:** Arch Linux
- **WM:** [Sway](https://swaywm.org/)
- **Shell:** zsh
- **Colorscheme:** [Nord](https://www.nordtheme.com/)
- **Notifications:** Mako
- **Terminal:** Alacritty
- **Browser:** Firefox + [Blurred Fox](https://github.com/manilarome/blurredfox)
- **Launcher:** [Sway Launcher](https://github.com/Biont/sway-launcher-desktop)
- **Spotify Client:** [Spicetify](https://github.com/khanhas/spicetify-cli)


<img src="https://gitlab.com/crayarc/delorean/-/raw/master/clean.png" width="800">
<img src="https://gitlab.com/crayarc/delorean/-/raw/master/sway-launch.png" width="800">
<img src="https://gitlab.com/crayarc/delorean/-/raw/master/dirty.png" width="800">
<img src="https://gitlab.com/crayarc/delorean/-/raw/master/spotify.png" width="800">
<img src="https://gitlab.com/crayarc/delorean/-/raw/master/ff.png" width="800">


